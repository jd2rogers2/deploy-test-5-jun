function Construct(props) {
  console.log(props.info);
  return (
    <div className="App">
      <ul>
        {props.info.map((row) => (
          <li key={row[0]}>
            id: {row[0]}, text: {row[1]}, int: {row[2]}, time created: {row[3]}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Construct;
