from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from psycopg_pool import ConnectionPool

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


@app.get("/")
def root():
    return {"message": "Hello World"}


@app.get("/api/get")
def get():
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT * FROM dummy;
            """
            )
            results = cur.fetchall()
            return results


@app.post("/api/post")
def post():
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                INSERT INTO dummy (
                    required_limited_text,
                    required_integer
                ) VALUES (
                    'test_text',
                    1
                )
                RETURNING *;
            """
            )
            result = cur.fetchone()
            return result
